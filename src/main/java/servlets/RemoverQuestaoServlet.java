package servlets;

import classes.Questao;
import dao.QuestaoDao;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;

@WebServlet("/removerQuestao")
public class RemoverQuestaoServlet extends HttpServlet {
    public RemoverQuestaoServlet() {
        super();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        QuestaoDao questaoDao = new QuestaoDao();

        Questao questao = questaoDao.findById(req.getParameter("idQuestao"), Questao.class);

        questaoDao.remove(questao, Questao.class);

        req.getRequestDispatcher("/carregaPaginaQuestao").forward(req, res);
    }
}
