package servlets;

import classes.Questao;
import classes.comparators.AcertosComparator;
import dao.QuestaoDao;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/carregaAdminPage")
public class CarregarAdminPageServelet extends HttpServlet {
    public CarregarAdminPageServelet() {
        super();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        QuestaoDao questaoDao = new QuestaoDao();

        List<Questao> listaQuestao = questaoDao.findAll(new Questao().getTableName());
        Comparator<Questao> comparator = new AcertosComparator();

        listaQuestao = listaQuestao.stream()
                .filter(p -> p.getTentativa() != null && p.getTentativa() > 0)
                .collect(Collectors.toList());

        Collections.sort(listaQuestao, comparator);

        Questao questoes[] = new Questao[15];

        for (int i = 0; i < 15; i++) {
            questoes[i] = listaQuestao.get(i);
        }

        req.setAttribute("questoes", questoes);
        req.getRequestDispatcher("admin.jsp").forward(req, res);
    }
}
