package filters;

import persistence.PersistenceManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(urlPatterns = {"/adicionarAulaProvaServlet",
        "/efetuaLogin",
        "/criarPaginaSimulado",
        "/carregarSimulado",
        "/marcarAulaServlet",
        "/carregaAdminPage",
        "/recebeNotificacao",
        "/inserirQuestao",
        "/removerQuestao",
        "/alterarQuestao",
        "/carregarPagAlterarQuestao"})
public class FiltroBanco implements Filter {

    private FilterConfig fconf = null;

    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        PersistenceManager.getInstance().initializeContext();

        chain.doFilter(request, response);

        PersistenceManager.getInstance().destroyContext();
    }

    @Override
    public void init(FilterConfig filterConfig) {
        fconf = filterConfig;
    }

    @Override
    public void destroy() {
        fconf = null;
    }
}