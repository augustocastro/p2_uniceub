package classes.comparators;

import classes.Questao;
import java.util.Comparator;

public class AcertosComparator implements Comparator<Questao> {
    @Override
    public int compare(Questao questao, Questao t1) {
        if(questao.getTentativa() == 0 && t1.getTentativa() == 0)
            return 0;

        if(questao.getTentativa() == 0 && t1.getTentativa() != 0)
            return 1;

        if(questao.getTentativa() != 0 && t1.getTentativa() == 0)
            return -1;

        Double porcentagem = (double)questao.getAcertos()/questao.getTentativa();
        Double porcentagem2 = (double)t1.getAcertos()/t1.getTentativa();

        return porcentagem.compareTo(porcentagem2);
    }
}
