package classes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "rl_cliente_prova")
public class RlClienteProva extends Entitable<RlClienteProva, Integer> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    private Cliente cliente;

    @ManyToOne
    private Prova prova;

    @Column(name = "aprovado")
    public Boolean aprovado;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Prova getProva() {
        return prova;
    }

    public void setProva(Prova prova) {
        this.prova = prova;
    }

    public Boolean getAprovado() {
        return aprovado;
    }

    public void setAprovado(Boolean aprovado) {
        this.aprovado = aprovado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RlClienteProva that = (RlClienteProva) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public void clonar(RlClienteProva objeto) {
        if (objeto.getAprovado() != null)
            setAprovado(objeto.getAprovado());

        if (objeto.getCliente() != null)
            setCliente(objeto.getCliente());

        if (objeto.getProva() != null)
            setProva(objeto.getProva());
    }
}
