package classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "notificacao")
public class Notificacao extends Entitable<Notificacao, Integer> implements Serializable {
    @Id
    private Integer id;

    @Column(name = "valor")
    private Double valor;

    @Column(name = "status")
    private String statusTransacao;

    @Column(name = "email_consumidor")
    private String emailConsumidor;

    @Column(name = "tipo_pagamento")
    private String tipoPagamento;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getStatusTransacao() {
        return statusTransacao;
    }

    public void setStatusTransacao(String statusTransacao) {
        this.statusTransacao = statusTransacao;
    }

    public String getEmailConsumidor() {
        return emailConsumidor;
    }

    public void setEmailConsumidor(String emailConsumidor) {
        this.emailConsumidor = emailConsumidor;
    }

    public String getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(String tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notificacao that = (Notificacao) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String getTableName() {
        return "Notificacao";
    }

    @Override
    public void clonar(Notificacao objeto) {
        if (objeto.getEmailConsumidor() != null)
            setEmailConsumidor(objeto.getEmailConsumidor());

        if (objeto.getStatusTransacao() != null)
            setStatusTransacao(objeto.getStatusTransacao());

        if (objeto.getTipoPagamento() != null)
            setTipoPagamento(objeto.getTipoPagamento());

        if (objeto.getValor() != null)
            setValor(objeto.getValor());
    }
}
