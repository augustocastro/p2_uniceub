package classes;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "servico")
public class Servico extends Entitable<Servico, Integer> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private TipoServico tipo;

    @ManyToOne
    private Cliente cliente;

    private Boolean pago;

    public Integer getId() {
        return id;
    }

    public void setId(Integer idServico) {
        this.id = idServico;
    }

    public TipoServico getTipo() {
        return tipo;
    }

    public void setTipo(TipoServico tipo) {
        this.tipo = tipo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Boolean getPago() {
        return pago;
    }

    public void setPago(Boolean pago) {
        this.pago = pago;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        Servico other = (Servico) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String getTableName() {
        return "Servico";
    }

    @Override
    public void clonar(Servico objeto) {
        if (objeto.getCliente() != null)
            setCliente(objeto.getCliente());

        if (objeto.getPago() != null)
            setPago(objeto.getPago());

        if (objeto.getTipo() != null)
            setTipo(objeto.getTipo());
    }

    ;
}
