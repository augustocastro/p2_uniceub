package classes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "cliente")
public class Cliente extends Entitable<Cliente, Integer> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "rg", length = 10)
    private String rg;

    @Column(name = "cpf", length = 20)
    private String cpf;

    @Column(name = "nome", length = 200, nullable = false)
    private String nome;

    @Column(name = "telefone", length = 25)
    private String telefone;

    @Column(name = "endereco", length = 200)
    private String endereco;

    @Column(name = "email", length = 100)
    private String email;

    @Temporal(TemporalType.DATE)
    @Column(name = "dataNascimento", columnDefinition = "date")
    private Date dataNascimento;

    @Column(name = "naturalidade", length = 20)
    private String naturalidade;

    @Column(name = "uf_naturalidade", length = 2)
    private String ufNaturalidade;

    @Column(name = "uf_endereco", length = 2)
    private String ufEndereco;

    @Column(name = "cidade", length = 45)
    private String cidade;

    @Column(name = "nome_mae", length = 100)
    private String nomeMae;

    @Column(name = "nome_pai", length = 100)
    private String nomePai;

    @Column(name = "cep", length = 20)
    private String cep;

    @Lob
    @Column(name = "crop_residencia", columnDefinition = "oid")
    private byte[] comprovanteResidencia;

    @Lob
    @Column(name = "cop_habilitacao", columnDefinition = "oid")
    private byte[] copiaHabilitacao;

    @OneToMany(mappedBy = "cliente", targetEntity = Servico.class, cascade = CascadeType.ALL)
    private Set<Servico> listaServicos;

    @OneToMany(mappedBy = "cliente")
    private Set<RlClienteAula> listaAula;

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
    private Set<RlClienteProva> listaProva;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getNaturalidade() {
        return naturalidade;
    }

    public void setNaturalidade(String naturalidade) {
        this.naturalidade = naturalidade;
    }

    public String getUfNaturalidade() {
        return ufNaturalidade;
    }

    public void setUfNaturalidade(String ufNaturalidade) {
        this.ufNaturalidade = ufNaturalidade;
    }

    public String getUfEndereco() {
        return ufEndereco;
    }

    public void setUfEndereco(String ufEndereco) {
        this.ufEndereco = ufEndereco;
    }

    public String getNomeMae() {
        return nomeMae;
    }

    public void setNomeMae(String nomeMae) {
        this.nomeMae = nomeMae;
    }

    public String getNomePai() {
        return nomePai;
    }

    public void setNomePai(String nomePai) {
        this.nomePai = nomePai;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public byte[] getComprovanteResidencia() {
        return comprovanteResidencia;
    }

    public void setComprovanteResidencia(byte[] comprovanteResidencia) {
        this.comprovanteResidencia = comprovanteResidencia;
    }

    public Set<RlClienteProva> getListaProva() {
        return listaProva;
    }

    public void setListaProva(Set<RlClienteProva> listaProva) {
        this.listaProva = listaProva;
    }

    public Set<RlClienteAula> getListaAula() {
        return listaAula;
    }

    public void setListaAula(Set<RlClienteAula> listaAula) {
        this.listaAula = listaAula;
    }

    public byte[] getCopiaHabilitacao() {
        return copiaHabilitacao;
    }

    public void setCopiaHabilitacao(byte[] copiaHabilitacao) {
        this.copiaHabilitacao = copiaHabilitacao;
    }

    public Set<Servico> getListaServicos() {
        return listaServicos;
    }

    public void setListaServicos(Set<Servico> listaServicos) {
        this.listaServicos = listaServicos;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        Cliente other = (Cliente) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;

        return true;
    }

    @Override
    public String getTableName() {
        return "Cliente";
    }

    @Override
    public void clonar(Cliente objeto) {
        if (objeto.getCidade() != null)
            this.setCidade(objeto.getCidade());

        if (objeto.getCep() != null)
            this.setCep(objeto.getCep());

        if (objeto.getNaturalidade() != null)
            this.setNaturalidade(objeto.getNaturalidade());

        if (objeto.getEmail() != null)
            this.setEmail(objeto.getEmail());

        if (objeto.getEndereco() != null)
            this.setEndereco(objeto.getEndereco());

        if (objeto.getTelefone() != null)
            this.setTelefone(objeto.getTelefone());

        if (objeto.getNome() != null)
            this.setNome(objeto.getNome());

        if (objeto.getRg() != null)
            this.setRg(objeto.getRg());

        if (objeto.getCpf() != null)
            this.setCpf(objeto.getCpf());

        if (objeto.getCopiaHabilitacao() != null)
            this.setCopiaHabilitacao(objeto.getCopiaHabilitacao());

        if (objeto.getComprovanteResidencia() != null)
            this.setComprovanteResidencia(objeto.getComprovanteResidencia());

        if (objeto.getDataNascimento() != null)
            this.setDataNascimento(objeto.getDataNascimento());

        if (objeto.getNomePai() != null)
            this.setNomePai(objeto.getNomePai());

        if (objeto.getNomeMae() != null)
            this.setNomeMae(objeto.getNomeMae());

        if (objeto.getUfEndereco() != null)
            this.setUfEndereco(objeto.getUfEndereco());

        if (objeto.getUfNaturalidade() != null)
            this.setUfNaturalidade(objeto.getUfNaturalidade());
    }

    ;
}
