package classes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "rl_cliente_aula")
public class RlClienteAula extends Entitable<RlClienteAula, Integer> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    private Cliente cliente;

    @ManyToOne
    private Aula aula;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Aula getAula() {
        return aula;
    }

    public void setAula(Aula aula) {
        this.aula = aula;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RlClienteAula that = (RlClienteAula) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String getTableName() {
        return "RlClienteAula";
    }

    @Override
    public void clonar(RlClienteAula objeto) {
        if (objeto.getAula() != null)
            this.setAula(objeto.getAula());

        if (objeto.getCliente() != null)
            this.setCliente(objeto.getCliente());
    }
}
