package classes;

public abstract class Entitable<T extends Entitable, V> {
    public abstract V getId();

    public String getTableName() {
        return "";
    }

    public abstract void clonar(T objeto);
}