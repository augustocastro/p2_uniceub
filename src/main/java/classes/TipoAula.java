package classes;

import classes.enums.TipoAulaNome;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "tipo_aula")
public class TipoAula extends Entitable<TipoAula, Integer> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "tipo", nullable = false)
    @Enumerated(EnumType.STRING)
    private TipoAulaNome tipoAula;

    @OneToMany(mappedBy = "tipoaula", cascade = CascadeType.ALL)
    private Set<Aula> listaAula;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoAulaNome getTipoAula() {
        return tipoAula;
    }

    public void setTipoAula(TipoAulaNome tipoAula) {
        this.tipoAula = tipoAula;
    }

    public Set<Aula> getListaAula() {
        return listaAula;
    }

    public void setListaAula(Set<Aula> listaAula) {
        this.listaAula = listaAula;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TipoAula tipoAula = (TipoAula) o;
        return Objects.equals(id, tipoAula.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String getTableName() {
        return "TipoAula";
    }

    @Override
    public void clonar(TipoAula objeto) {
        if(objeto.getTipoAula() != null)
            setTipoAula(objeto.getTipoAula());
    }
}
