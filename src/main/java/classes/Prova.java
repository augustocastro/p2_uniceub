package classes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "prova")
public class Prova extends Entitable<Prova, Date> implements Serializable {
    @Id
    @Temporal(TemporalType.DATE)
    @Column(columnDefinition = "date")
    private Date id;

    @OneToMany(mappedBy = "prova", cascade = CascadeType.ALL)
    private Set<RlClienteProva> listaProva;

    @Override
    public Date getId() {
        return id;
    }

    public void setId(Date id) {
        this.id = id;
    }

    public Set<RlClienteProva> getListaProva() {
        return listaProva;
    }

    public void setListaProva(Set<RlClienteProva> listaProva) {
        this.listaProva = listaProva;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Prova prova = (Prova) o;
        return Objects.equals(id, prova.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String getTableName() {
        return "Prova";
    }

    @Override
    public void clonar(Prova objeto) {
        setId(objeto.getId());
    }
}