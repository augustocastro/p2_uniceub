package dao;

import classes.Aula;
import classes.Cliente;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.EntityTransaction;
import javax.persistence.EntityManager;

public class ClienteDao extends DaoGeral<Cliente, Integer> {
    public ClienteDao() {
        super();
    }

    public Integer findIdByRg(String rg) {
        Integer retorno;

        EntityManager entityManager = manager.getEntityManager();

        final EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();

        String sql = "select p from Cliente p where rg = :rgparametro";
        Query query = entityManager.createQuery(sql);
        query.setParameter("rgparametro", rg);

        List<Cliente> listaRetorno = query.getResultList();

        if(listaRetorno.size() == 0){
            retorno = -1;
        } else {
            retorno = listaRetorno.get(0).getId();
        }

        transaction.commit();

        entityManager.close();

        return retorno;
    }
}