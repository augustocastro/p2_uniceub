package dao;

import classes.Questao;
import classes.Simulado;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class SimuladoDao extends DaoGeral<Simulado, Integer> {
    public SimuladoDao() {
        super();
    }

    public List<Questao> carregarListaQuestao(Integer id) {
        EntityManager entityManager = manager.getEntityManager();

        final EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();

        Query query = entityManager.createQuery("from Questao a join fetch a.listaSimulado b where b.id = :parametro");
        query.setParameter("parametro", id);

        List<Questao> listaQuestao = query.getResultList();

        transaction.commit();

        entityManager.close();

        return listaQuestao;
    }
}
