<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
    <title>Gráficos de Resultados</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="css/admin.css">
    <link rel="stylesheet" href="css/base.css">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages': ['corechart']});

        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Id');
            data.addColumn('number', 'Porcentagem');
            data.addRows([
                <c:forEach var="questao" items="${requestScope.questoes}" varStatus="count">
                ['${count.count}', ${100.0*questao.acertos/questao.tentativa}],
                </c:forEach>
            ]);

            var options = {
                'title': 'Id-Questão x Acerto%',
                'width': 800,
                'height': 500
            };

            var chart = new google.visualization.BarChart(document.getElementById('chart-container'));
            chart.draw(data, options);
        }
    </script>

</head>

<body>

<!-- Navbar -->
<div class="w3-top">
    <div class="w3-bar w3-blue w3-card w3-left-align w3-large">
        <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red"
           href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fas fa-bars"></i></a>

        <a href="/carregaAdminPage" class="w3-bar-item w3-button w3-padding-large w3-white link-navbar">
            <i class="fas fa-chart-bar fa-2x"></i>
        </a>

        <a href="/carregaNotificacao"
           class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white link-navbar">
            <i class="fas fa-dollar-sign fa-2x"></i>
        </a>

        <a href="/carregaPaginaQuestao"
           class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white link-navbar">
            <i class="fas fa-question fa-2x"></i>
        </a>

        <a href="home.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white link-navbar">
            <i class="fas fa-power-off fa-2x"></i>
        </a>
    </div>

    <!-- Navbar on small screens -->
    <div id="navDemo" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium w3-large">
        <a href="#" class="w3-bar-item w3-button w3-padding-large">
            Serviços
        </a>

        <a href="#" class="w3-bar-item w3-button w3-padding-large">
            Apostilas
        </a>

        <a href="#" class="w3-bar-item w3-button w3-padding-large">
            Simulados
        </a>

        <a href="#" class="w3-bar-item w3-button w3-padding-large">
            Fale Conosco
        </a>
    </div>
</div>

<!-- Header -->
<header class="w3-container w3-blue w3-center" style="padding:128px 16px">
    <h1 class="w3-margin w3-jumbo">Gráfico dos Resultados das Questões</h1>
    <p class="w3-xlarge">As 15 questões com os menores índices de acerto</p>
</header>

<!-- First Grid -->
<div class="w3-row-padding w3-padding-64 w3-container">
    <div class="w3-content w3-center" id="chart-container">
    </div>
</div>

<!-- Second Grid -->
<div class="w3-row-padding w3-light-grey w3-padding-64 w3-container">
    <div class="w3-content">
        <div class="w3-center">
            <h2>Legenda</h2>
            <table id="tabela-legenda">
                <tbody id="legenda">
                <c:forEach varStatus="count" var="questao" items="${requestScope.questoes}">
                    <tr class="linha-tabela">
                        <td>
                            <p>Id: ${count.count}</p>
                        </td>
                        <td>
                            <p>${questao.id}</p>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <div class="w3-center w3-bar" id="myPager">
            </div>
        </div>
    </div>
</div>

<div class="w3-container w3-black w3-center w3-opacity w3-padding-64">
    <h2 class="w3-margin w3-xlarge">jasseferreira@gmail.com - (61)3351-1971</h2>
</div>

<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-opacity">
    <div class="w3-xlarge w3-padding-32">
        <a href="https://www.facebook.com/Arraisdf/">
            <i class="fab fa-facebook w3-hover-opacity"></i>
        </a>
        <a href="https://www.instagram.com/escolanautica_arraisdf/">
            <i class="fab fa-instagram w3-hover-opacity"></i>
        </a>
    </div>

    <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
</footer>
<script type="text/javascript" src="js/paginainicial.js"></script>
<script type="text/javascript" src="js/paginacao.js"></script>
</body>
</html>
